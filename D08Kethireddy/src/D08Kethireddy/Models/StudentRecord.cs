﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;


namespace D08Kethireddy.Models
{
    public class StudentRecord
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Student 919#")]
        [Key]
        public int studentID { get; set; }

        [Required]
        [Display(Name = "Student Snumber")]
        public string studentSnumber { get; set; }

        [Display(Name = "Last Name")]
        public string lastName { get; set; }

        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [EmailAddress]
        [RegularExpression(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.+-]+\.edu$")]
        [Display(Name = "Email")]
        public String email { get; set; }

        [RegularExpression(@"^\d{10}$")]
        [Phone]
        [Display(Name = "Phone")]
        public String phone { get; set; }

        public int? LocationID { get; set; }

        [Display(Name = "Location")]
        public virtual Location Location { get; set; }

        public List<Location> studentrecorder { get; set; }

        public static List<StudentRecord> ReadAllFromCSV(string filepath)
        {
            List<StudentRecord> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => StudentRecord.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static StudentRecord OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            StudentRecord item = new StudentRecord();
            int i = 0;
            item.studentID = Convert.ToInt32(values[i++]);
            item.studentSnumber = Convert.ToString(values[i++]);
            item.lastName = Convert.ToString(values[i++]);
            item.firstName = Convert.ToString(values[i++]);
            item.email = Convert.ToString(values[i++]);
            item.phone = Convert.ToString(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);
            return item;
        }
    }
}
