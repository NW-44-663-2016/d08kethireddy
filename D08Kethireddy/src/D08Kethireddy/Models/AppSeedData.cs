﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using D08Kethireddy.Models;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace D08Kethireddy.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            context.StudentRecords.RemoveRange(context.StudentRecords);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedStudentRecordsFromCsv(relPath, context);
        }

       

        private static void SeedStudentRecordsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "studentrecord.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            StudentRecord.ReadAllFromCSV(source);

            List<StudentRecord> lst = StudentRecord.ReadAllFromCSV(source);
            context.StudentRecords.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }

    }
}

