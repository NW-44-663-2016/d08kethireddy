﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace D08Kethireddy.Models
{
    public class AppDbContext : DbContext
    {
       public DbSet<Location> Locations { get; set; }
       public DbSet<StudentRecord> StudentRecords { get; set; }
    }
}
