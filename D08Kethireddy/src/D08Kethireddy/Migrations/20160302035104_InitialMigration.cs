using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace D08Kethireddy.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    LocationID = table.Column<int>(nullable: false),
                    Country = table.Column<string>(nullable: true),
                    County = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Place = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    StateAbbreviation = table.Column<string>(nullable: true),
                    StudentRecordstudentID = table.Column<int>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.LocationID);
                });
            migrationBuilder.CreateTable(
                name: "StudentRecord",
                columns: table => new
                {
                    studentID = table.Column<int>(nullable: false),
                    LocationID = table.Column<int>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    firstName = table.Column<string>(nullable: true),
                    lastName = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    studentSnumber = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentRecord", x => x.studentID);
                    table.ForeignKey(
                        name: "FK_StudentRecord_Location_LocationID",
                        column: x => x.LocationID,
                        principalTable: "Location",
                        principalColumn: "LocationID",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_Location_StudentRecord_StudentRecordstudentID",
                table: "Location",
                column: "StudentRecordstudentID",
                principalTable: "StudentRecord",
                principalColumn: "studentID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_StudentRecord_Location_LocationID", table: "StudentRecord");
            migrationBuilder.DropTable("Location");
            migrationBuilder.DropTable("StudentRecord");
        }
    }
}
