using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08Kethireddy.Models;

namespace D08Kethireddy.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08Kethireddy.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<int?>("StudentRecordstudentID");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08Kethireddy.Models.StudentRecord", b =>
                {
                    b.Property<int>("studentID");

                    b.Property<int?>("LocationID");

                    b.Property<string>("email");

                    b.Property<string>("firstName");

                    b.Property<string>("lastName");

                    b.Property<string>("phone");

                    b.Property<string>("studentSnumber")
                        .IsRequired();

                    b.HasKey("studentID");
                });

            modelBuilder.Entity("D08Kethireddy.Models.Location", b =>
                {
                    b.HasOne("D08Kethireddy.Models.StudentRecord")
                        .WithMany()
                        .HasForeignKey("StudentRecordstudentID");
                });

            modelBuilder.Entity("D08Kethireddy.Models.StudentRecord", b =>
                {
                    b.HasOne("D08Kethireddy.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
