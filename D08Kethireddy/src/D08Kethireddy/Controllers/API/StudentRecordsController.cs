using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using D08Kethireddy.Models;

namespace D08Kethireddy.Controllers
{
    [Produces("application/json")]
    [Route("api/StudentRecords")]

    public class StudentRecordsController : Controller
    {
        private AppDbContext _context;

        public StudentRecordsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: api/StudentRecords
        [HttpGet]
        public IEnumerable<StudentRecord> GetStudentRecords()
        {
            return _context.StudentRecords;
        }


        // GET: api/StudentRecords/5
        [HttpGet("{id}", Name = "GetStudentRecord")]

        public IActionResult GetStudentRecord([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }


            StudentRecord studentRecord = _context.StudentRecords.Single(m => m.studentID == id);
            if (studentRecord == null)
            {
                return HttpNotFound();
            }

            return Ok(studentRecord);
        }

        // PUT: api/StudentRecords/5
        [HttpPut("{id}")]
        public IActionResult PutStudentRecord(int id, [FromBody] StudentRecord StudentRecord)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }
            if (id != StudentRecord.studentID)
            {
                return HttpBadRequest();
            }

            _context.Entry(StudentRecord).State = EntityState.Modified;
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentRecordExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/StudentRecords
        [HttpPost]
        public IActionResult PostCar([FromBody] StudentRecord StudentRecord)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.StudentRecords.Add(StudentRecord);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (StudentRecordExists(StudentRecord.studentID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetStudentRecord", new { id = StudentRecord.studentID }, StudentRecord);
        }



        // DELETE: api/StudentRecords/5
        [HttpDelete("{id}")]
        public IActionResult DeleteStudentRecord(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            StudentRecord StudentRecord = _context.StudentRecords.Single(m => m.studentID == id);
            if (StudentRecord == null)
            {
                return HttpNotFound();
            }

            _context.StudentRecords.Remove(StudentRecord);
            _context.SaveChanges();

            return Ok(StudentRecord);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StudentRecordExists(int id)
        {
            return _context.StudentRecords.Count(e => e.studentID == id) > 0;
        }
    }
}
